package me.nicholas.macrobot.preconditions

import me.jakejmattson.kutils.api.annotations.Precondition
import me.jakejmattson.kutils.api.dsl.preconditions.*
import me.jakejmattson.kutils.api.dsl.arguments.*
import me.jakejmattson.kutils.api.services.ScriptEngineService

import me.nicholas.macrobot.services.*
import me.nicholas.macrobot.util.*

@Precondition(0)
fun isCommandInvocation(
        databaseService: DatabaseService,
        permissionService: PermissionService
) = precondition {
    it.command ?: run {
        val message = it.message
        val member = it.message.member!!

        val prefixSeq = generateSequence(it.relevantPrefix) { x -> x + it.relevantPrefix }

        val prefixBlock = prefixSeq
                .takeWhile { message.contentRaw.startsWith(it) }
                .last()

        val messageContent = message.contentRaw
                .removePrefix(prefixBlock)
                .split(' ')

        val commandName = messageContent.first()
        var args = messageContent.drop(1)

        val macro = databaseService.macros[commandName]
                ?: return@precondition Fail()

        val macroCategoryPermissions = databaseService.permissionOverrides[macro.category]!!

        if (!permissionService.hasClearance(member, macroCategoryPermissions)) {
            return@precondition Fail("You are lacking permissions to invoke specified macro")
        }

        if (!permissionService.checkLimitsForOrigin(message, macroCategoryPermissions)) {
            return@precondition Fail("This channel is out of bounds for specified macro")
        }

        val engine = it.discord.getInjectionObjects(ScriptEngineService::class).engine
        var messageDisplay = macro.message

        engine.put("event", it)
        macro.specialArguments.forEachIndexed { argPos, specialArg ->
            if (specialArg.isRequired) {
                if (args.isEmpty()) {
                    return@precondition Fail("Expected more arguments")
                }
            } else {
                if (specialArg.defaultValue!!.isEmpty() && args.isEmpty()) {
                    return@forEachIndexed specialArg.transforms.forEachIndexed { transformPos, _ ->
                        messageDisplay = messageDisplay.replace("{${argPos}.${transformPos}}", "")
                    }
                }
            }

            engine.put("args", args.ifEmpty { specialArg.defaultValue })

            val result = engine.eval("""
                import me.jakejmattson.kutils.api.dsl.command.*
                import me.nicholas.macrobot.extensions.*
                
                val args = bindings["args"] as List<String>
                val event = bindings["event"] as CommandEvent<*>
                
                val result = ${specialArg.typename}.convert(args.first(), args, event)
                val obj = result.maybeConverted()
                    
                result
            """.trimIndent()) as ArgumentResult<*>

            when (result) {
                is ArgumentResult.Error -> return@precondition Fail(result.error)
                is ArgumentResult.Success -> { args = args.drop(result.consumed) }
            }

            specialArg.transforms.forEachIndexed { transformPos, transform ->
                engine.put("message", messageDisplay)

                val replacement = runCatching {
                    engine.eval("obj!!${transform}.toString()") as String
                }.getOrElse { return@precondition Fail(it.localizedMessage) }

                messageDisplay = messageDisplay.replace("{${argPos}.${transformPos}}", replacement)
            }
        }

        if (args.isNotEmpty()) {
            return@precondition Fail("Got more arguments than expected")
        }

        it.respond(messageDisplay)

        return@precondition Fail()
    }

    return@precondition Pass
}