package me.nicholas.macrobot.preconditions

import me.jakejmattson.kutils.api.annotations.Precondition
import me.jakejmattson.kutils.api.dsl.preconditions.*

import me.nicholas.macrobot.services.*

@Precondition(1)
fun hasClearanceToInvokeCommand(permissionService: PermissionService) = precondition {
    val message = it.message
    val member = it.message.member!!
    val command = it.command!!
    val commandPermissions = permissionService.getOrPutDefaultCommandPermission(command)

    if (!permissionService.hasClearance(member, commandPermissions)) {
        return@precondition Fail("You are lacking permissions to invoke specified command")
    }

    if (!permissionService.checkLimitsForOrigin(message, commandPermissions)) {
        return@precondition Fail("This channel is out of bounds for specified command")
    }

    return@precondition Pass
}