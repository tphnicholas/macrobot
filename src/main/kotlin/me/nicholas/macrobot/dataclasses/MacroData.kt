package me.nicholas.macrobot.dataclasses

import kotlinx.serialization.Serializable

typealias MacroName = String

@Serializable
data class MacroData(
        var category: String,
        var message: String,
        var specialArguments: List<SpecialArgument> = emptyList()
)