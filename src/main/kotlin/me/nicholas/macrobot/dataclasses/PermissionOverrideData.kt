package me.nicholas.macrobot.dataclasses

import kotlinx.serialization.*

typealias OverrideName = String
typealias Limits = MutableSet<OverrideName>

@Polymorphic
@Serializable
abstract class PermissionOverrideData() {
    abstract var requiredPermissionLevel: PermissionLevel?
    abstract var channelLimits: Limits
    abstract var categoryLimits: Limits

    fun clearAllLimits() {
        channelLimits.clear()
        categoryLimits.clear()
    }

    fun hasAnyLimitsSet() = (channelLimits.isNotEmpty() || categoryLimits.isNotEmpty())
    fun isChannelWithinLimits(overrideName: OverrideName?) = channelLimits.contains(overrideName)
    fun isCategoryWithinLimits(categoryId: OverrideName?) = categoryLimits.contains(categoryId)
}

@Serializable
@SerialName("CommandPermission")
class CommandPermission(
        override var requiredPermissionLevel: PermissionLevel? = null,
        override var channelLimits: Limits = mutableSetOf(),
        override var categoryLimits: Limits = mutableSetOf()
) : PermissionOverrideData() {

}

@Serializable
@SerialName("MacroCategoryPermission")
class MacroCategoryPermission(
        override var requiredPermissionLevel: PermissionLevel? = null,
        override var channelLimits: Limits = mutableSetOf(),
        override var categoryLimits: Limits = mutableSetOf()
) : PermissionOverrideData() {

}