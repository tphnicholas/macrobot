package me.nicholas.macrobot.dataclasses

import kotlinx.serialization.Serializable

typealias PermissionLevel = Int

@Serializable
data class PermissionData(
        var level: PermissionLevel = 0
)