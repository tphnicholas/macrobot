package me.nicholas.macrobot.dataclasses

import kotlinx.serialization.Serializable

@Serializable
class SpecialArgument(
        val typename: String,
        val transforms: MutableSet<String> = mutableSetOf(),
        val defaultValue: List<String>? = null
) {
    val isRequired = (defaultValue == null)
}