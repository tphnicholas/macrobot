package me.nicholas.macrobot.util

object Constants {
    const val dataDir = "persistence"
    const val macrodb = "$dataDir/macrodb.json"
    const val permissionOverridesdb = "$dataDir/permissionOverridesdb.json"
    const val permissiondb = "$dataDir/permissiondb.json"
    const val configFile = "$dataDir/config.json"

    const val pathToKutilsArgs = "me.jakejmattson.kutils.api.arguments"
    const val pathToMacrobotArgs = "me.nicholas.macrobot.arguments"
}