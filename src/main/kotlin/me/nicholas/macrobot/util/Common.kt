package me.nicholas.macrobot.util

import org.reflections.Reflections
import me.jakejmattson.kutils.api.dsl.arguments.ArgumentType

val argumentTypeRegex = Regex(".*\\.([\\w\\d]+)\$")
val specialArgumentSubscriptRegex = Regex("\\{\\s*(\\d+)(.*?)\\s*\\}")

val argumentTypeNames
        = Reflections(Constants.pathToKutilsArgs).getSubTypesOf(ArgumentType::class.java)
        .plus(Reflections(Constants.pathToMacrobotArgs).getSubTypesOf(ArgumentType::class.java))
        .map { argumentTypeRegex.find(it.name) }
        .filterNotNull()
        .filter {
            val (qualifiedName, _) = it.groupValues
            Class.forName(qualifiedName).kotlin.typeParameters.isEmpty()
        }

fun<T> bold(obj: T) = "**${obj.toString()}**"
fun<T> block(obj: T) = "```css\n${obj.toString()}```"
fun<T> blockInline(obj: T) = "``${obj.toString()}``"

fun getArgumentTypeName(name: String) : Pair<String, String>? {
    val (qualifiedName, className) = argumentTypeNames
            .find {
                val (qualifiedName, className) = it.groupValues
                className.equals(name, ignoreCase = true) || qualifiedName.equals(name, ignoreCase = true)
            }?.groupValues
            ?: return null

    return Pair(qualifiedName, className)
}