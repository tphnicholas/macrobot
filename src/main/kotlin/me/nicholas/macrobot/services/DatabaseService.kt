package me.nicholas.macrobot.services

import java.io.File
import kotlinx.serialization.*
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import me.jakejmattson.kutils.api.annotations.Service

import me.nicholas.macrobot.dataclasses.*
import me.nicholas.macrobot.util.Constants

typealias RoleId = String

private val permissionOverrideModule = SerializersModule {
    polymorphic(PermissionOverrideData::class) {
        CommandPermission::class with CommandPermission.serializer()
        MacroCategoryPermission::class with MacroCategoryPermission.serializer()
    }
}

@Service
class DatabaseService {
    val macros = loadMacros()
    val permissionOverrides = loadPermissionOverrides()
    val permissions = loadPermissions()

    private fun loadMacros() : HashMap<MacroName, MacroData> {
        @OptIn(ImplicitReflectionSerializer::class)
        val result = Json.parseMap<MacroName, MacroData>(getOrCreateFile(Constants.macrodb).readText())

        return ((result as HashMap<MacroName, MacroData>?) ?: HashMap())
    }

    private fun loadPermissionOverrides() : HashMap<OverrideName, PermissionOverrideData> {
        @OptIn(ImplicitReflectionSerializer::class)
        val result = Json(context = permissionOverrideModule)
                .parseMap<OverrideName, PermissionOverrideData>(
                        getOrCreateFile(Constants.permissionOverridesdb).readText())

        return ((result as HashMap<OverrideName, PermissionOverrideData>?) ?: HashMap())
    }

    private fun loadPermissions() : HashMap<RoleId, PermissionData> {
        @OptIn(ImplicitReflectionSerializer::class)
        val result = Json.parseMap<RoleId, PermissionData>(getOrCreateFile(Constants.permissiondb).readText())

        return ((result as HashMap<RoleId, PermissionData>?) ?: HashMap())
    }

    private fun getOrCreateFile(filename: String)
            = File(filename)
            .also {
                if (it.createNewFile()) {
                    it.writeText("{}")
                }
            }

    private fun saveSnapshot() {
        @OptIn(ImplicitReflectionSerializer::class)
        File(Constants.macrodb)
                .writeText(Json.stringify(macros))

        @OptIn(ImplicitReflectionSerializer::class)
        File(Constants.permissionOverridesdb)
                .writeText(Json(context = permissionOverrideModule).stringify(permissionOverrides))

        @OptIn(ImplicitReflectionSerializer::class)
        File(Constants.permissiondb)
                .writeText(Json.stringify(permissions))
    }

    private fun onModified() = saveSnapshot()

    fun applyUpdate(modification: DatabaseService.() -> Unit) {
        modification(this)
        onModified()
    }

    fun hasMacroWithName(name: String) = macros.keys.any { it.equals(name, ignoreCase = true) }
}