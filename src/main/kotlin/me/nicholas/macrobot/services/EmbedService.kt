package me.nicholas.macrobot.services

import net.dv8tion.jda.api.entities.*
import me.jakejmattson.kutils.api.annotations.Service
import me.jakejmattson.kutils.api.dsl.arguments.ArgumentType
import me.jakejmattson.kutils.api.dsl.command.*
import me.jakejmattson.kutils.api.dsl.embed.*

import me.nicholas.macrobot.dataclasses.*
import me.nicholas.macrobot.extensions.*
import me.nicholas.macrobot.util.*

@Service
class EmbedService(private val databaseService: DatabaseService) {
    fun buildHelpEmbed(
            commands: List<Command>,
            macros: Map<MacroName, MacroData>,
            event: CommandEvent<*>
    ) = buildInvocableEmbed(commands, macros)
            .toEmbedBuilder()
            .also {
                it.setTitle(bold("Help menu"))
                it.setDescription("Use ${blockInline("${event.relevantPrefix}help <command>")} for more information.")
            }
            .build()

    fun buildInvocableEmbed(
            commands: List<Command>,
            macros: Map<MacroName, MacroData>
    ) = embed {
        commands
                .sortedByDescending { x -> commands.count { it.category == x.category } }
                .groupBy { it.category }
                .forEach { (category, commandList) ->
                    field {
                        name = category.toString()
                        value = block(commandList
                                .map { it.nameInLowercase() }
                                .sorted()
                                .joinToString("\n")
                        )
                        inline = true
                    }
                }

        macros
                .toList()
                .sortedByDescending { x -> macros.count { it.value.category == x.second.category} }
                .groupBy { (_, v) -> v.category }
                .forEach { (category, macroList) ->
                    field {
                        name = category.toString()
                        value = block(macroList
                                .map { (name, _) -> name }
                                .sorted()
                                .joinToString("\n")
                        )
                        inline = true
                    }
                }
        color = infoColor
    }

    fun buildPermissionLevelEmbed(
            permissions: Map<String, PermissionData>,
            guild: Guild
    ) = embed {
        field {
            name = "Permission Levels"
            value = permissions
                    .toList()
                    .groupBy { (_, v) -> v.level }
                    .toSortedMap(compareByDescending { it })
                    .map { (permissionLevel, permissionList) ->
                        "${permissionLevel}: ${permissionList
                                .map { (roleId, _) -> guild.getRoleById(roleId)!!.name }
                                .sorted()
                                .joinToString(", ")
                        }"
                    }
                    .joinToString("\n")
        }
        color = infoColor
    }

    fun buildCommandHelpEmbed(command: Command, event: CommandEvent<*>) = embed {
        val commandName = command.nameInLowercase()

        val commandInvocation = "${event.relevantPrefix}${commandName}"

        val args = command.arguments
                .joinToString(" ") { if (it.isOptional) "(${it.name})" else "[${it.name}]" }

        val exampleArgs = command.arguments
                .joinToString(" ") { it.generateExamples(event).random() }

        title = bold("Displaying help for $commandName")
        description = command.description

        field {
            name = "What is the structure of the command?"
            value = "$commandInvocation $args"
        }

        field {
            name = "Show me an example of someone using the command."
            value = "$commandInvocation $exampleArgs"
        }

        buildVisibleLimitsField(commandName, event)

        color = infoColor
    }

    fun buildMacroHelpEmbed(macroName: String, event: CommandEvent<*>) = embed {
        val macro = databaseService.macros[macroName]!!

        val macroInvocation = "${event.relevantPrefix}${macroName}"

        val argumentTypeNames = macro.specialArguments
                .map { getArgumentTypeName(it.typename)!! }

        val args = macro.specialArguments
                .mapIndexed { pos, x ->
                    val (_, argName) = argumentTypeNames[pos]

                    if (x.isRequired) {
                        "[${argName}]"
                    } else if (x.defaultValue!!.isNotEmpty()) {
                        "(${argName})"
                    } else {
                        "((${argName}))"
                    }
                }
                .joinToString(" ")

        val exampleArgs = argumentTypeNames
                .joinToString(" ") { (qualifiedName, _) ->
                    val type = Class.forName(qualifiedName).newInstance() as ArgumentType<*>

                    type.generateExamples(event).random()
                }

        title = bold("Displaying help for $macroName")

        field {
            name = "What is the structure of the command?"
            value = "$macroInvocation $args"
        }

        field {
            name = "Show me an example of someone using the command."
            value = "$macroInvocation $exampleArgs"
        }

        buildVisibleLimitsField(macro.category, event)

        color = infoColor
    }
}