package me.nicholas.macrobot.services

import net.dv8tion.jda.api.entities.*
import me.jakejmattson.kutils.api.annotations.Service
import me.jakejmattson.kutils.api.dsl.command.Command

import me.nicholas.macrobot.data.Configuration
import me.nicholas.macrobot.dataclasses.*
import me.nicholas.macrobot.extensions.*

@Service
class PermissionService(
        private val databaseService: DatabaseService,
        private val configuration: Configuration
) {
    val lowestPermissionLevel = 0
    val highestPermissionLevel = 100
    val defaultCommandPermissionLevel = highestPermissionLevel

    val defaultCommandPermission = configuration.staffRoleId

    init {
        databaseService.applyUpdate {
            permissions.putIfAbsent(
                    defaultCommandPermission,
                    PermissionData(defaultCommandPermissionLevel)
            )
        }
    }

    fun hasPermission(
            highestUserPermission: PermissionLevel?,
            minimumRequiredPermission: PermissionLevel?
    ) : Boolean {
        if (minimumRequiredPermission == null) { return true }

        if ((highestUserPermission != null) && (highestUserPermission >= minimumRequiredPermission)) {
            return true
        }

        return false
    }

    fun hasClearance(member: Member, permissionOverrideData: PermissionOverrideData)
            = hasClearance(member, permissionOverrideData.requiredPermissionLevel)

    fun hasClearance(member: Member, minimumRequiredPermission: PermissionLevel?)
            = hasPermission(getMemberPermissionLevel(member), minimumRequiredPermission)

    fun getMemberPermissionLevel(member: Member) : PermissionLevel? {
        val userRoleIds = member.roles.map { it.id }

        return databaseService.permissions
                .filterKeys { it in userRoleIds }
                .map { it.value.level }
                .max()
    }

    fun getDefaultCommandPermission() = databaseService.permissions[defaultCommandPermission]!!

    fun isDefaultCommandPermission(role: Role) = (role.id == defaultCommandPermission)

    fun getOrPutDefaultCommandPermission(command: Command) = getOrPutDefaultCommandPermission(command.nameInLowercase())

    fun getOrPutDefaultCommandPermission(command: String)
            = databaseService.permissionOverrides
            .getOrPut(command, { CommandPermission(getDefaultCommandPermission().level) })

    fun resetPermissionLevel(permission: PermissionOverrideData) {
        permission.requiredPermissionLevel = null
    }

    fun maybeRemovePermission(roleId: RoleId?) = roleId?.let { removePermission(it) }

    fun modifyPermissionLevel(roleId: RoleId, newLevel: PermissionLevel) {
        val existingPermission = databaseService.permissions[roleId]!!

        val oldPermissionLevel = existingPermission.level

        existingPermission.level = newLevel

        databaseService.permissions.values.firstOrNull { it.level == oldPermissionLevel }
                ?: databaseService.permissionOverrides
                        .filterValues { it.requiredPermissionLevel == oldPermissionLevel }
                        .values.forEach { it.requiredPermissionLevel = newLevel }
    }

    fun removePermission(roleId: RoleId) {
        val removedPermission = databaseService.permissions.remove(roleId)!!

        databaseService.permissions.values.firstOrNull { it.level == removedPermission.level }
                ?: databaseService.permissionOverrides
                        .filterValues { it.requiredPermissionLevel == removedPermission.level }
                        .values.forEach { resetPermissionLevel(it) }
    }

    fun checkLimitsFor(
            channel: MessageChannel,
            category: Category?,
            permission: PermissionOverrideData
    ) : Boolean {
        if (!permission.hasAnyLimitsSet()) { return true }

        val isChannelWithinLimits = permission.isChannelWithinLimits(channel.id)
        val isCategoryWithinLimits = permission.isCategoryWithinLimits(category?.id)

        return (isChannelWithinLimits || isCategoryWithinLimits)
    }

    fun checkLimitsForOrigin(message: Message, permission: PermissionOverrideData)
            = checkLimitsFor(message.channel, message.category, permission)
}