package me.nicholas.macrobot.arguments

import me.jakejmattson.kutils.api.dsl.arguments.*
import me.jakejmattson.kutils.api.dsl.command.*

import me.nicholas.macrobot.dataclasses.MacroCategoryPermission
import me.nicholas.macrobot.services.DatabaseService
import me.nicholas.macrobot.util.bold

open class MacroCategoryArg(override val name : String = "Macro Category") : ArgumentType<String>() {
    companion object : MacroCategoryArg()

    override fun generateExamples(event: CommandEvent<*>) = listOf("moderation", "utility", "fun")

    override fun convert(arg: String, args: List<String>, event: CommandEvent<*>): ArgumentResult<String> {
        val firstWord = arg.toLowerCase()

        val databaseService = event.discord.getInjectionObjects(DatabaseService::class)

        return when (databaseService.permissionOverrides[firstWord]) {
            is MacroCategoryPermission -> ArgumentResult.Success(firstWord, 1)
            else -> ArgumentResult.Error("${bold(firstWord)} is not a valid macro category")
        }
    }
}