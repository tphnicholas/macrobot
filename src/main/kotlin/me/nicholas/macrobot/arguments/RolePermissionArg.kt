package me.nicholas.macrobot.arguments

import net.dv8tion.jda.api.entities.Role
import me.jakejmattson.kutils.api.arguments.RoleArg
import me.jakejmattson.kutils.api.dsl.arguments.*
import me.jakejmattson.kutils.api.dsl.command.*

import me.nicholas.macrobot.extensions.map
import me.nicholas.macrobot.services.DatabaseService
import me.nicholas.macrobot.util.bold

open class RolePermissionArg(override val name : String = "Role Permission") : ArgumentType<Role>() {
    companion object : RolePermissionArg()

    override fun generateExamples(event: CommandEvent<*>) = listOf("Staff", "<@292033406697734144>", "292033406697734144")

    override fun convert(arg: String, args: List<String>, event: CommandEvent<*>): ArgumentResult<Role> {
        val databaseService = event.discord.getInjectionObjects(DatabaseService::class)

        return RoleArg.convert(arg, args, event).map {
            databaseService.permissions[it.id]
                    ?: return ArgumentResult.Error("${bold(arg)} is not a valid role permission")
        }
    }
}