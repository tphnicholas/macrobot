package me.nicholas.macrobot.arguments

import me.jakejmattson.kutils.api.arguments.IntegerArg
import me.jakejmattson.kutils.api.dsl.arguments.*
import me.jakejmattson.kutils.api.dsl.command.*

import me.nicholas.macrobot.services.*
import me.nicholas.macrobot.dataclasses.PermissionLevel
import me.nicholas.macrobot.extensions.map
import me.nicholas.macrobot.util.bold

open class PermissionLevelArg(override val name : String = "Permission Level") : ArgumentType<PermissionLevel>() {
    companion object : PermissionLevelArg()

    override fun generateExamples(event: CommandEvent<*>) = listOf("0", "1", "2")

    override fun convert(arg: String, args: List<String>, event: CommandEvent<*>): ArgumentResult<PermissionLevel> {
        val permissionService = event.discord.getInjectionObjects(PermissionService::class)

        return IntegerArg.convert(arg, args, event).map {
            with (permissionService) {
                if (it < lowestPermissionLevel) {
                    return ArgumentResult.Error(
                            "Permission level cannot be lower than ${bold(lowestPermissionLevel)}"
                    )
                }
                if (it > highestPermissionLevel) {
                    return ArgumentResult.Error(
                            "Permission level cannot be higher than ${bold(highestPermissionLevel)}"
                    )
                }
            }
        }
    }
}