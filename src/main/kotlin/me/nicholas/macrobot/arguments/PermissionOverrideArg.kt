package me.nicholas.macrobot.arguments

import me.jakejmattson.kutils.api.dsl.arguments.*
import me.jakejmattson.kutils.api.dsl.command.*

import me.nicholas.macrobot.dataclasses.OverrideName
import me.nicholas.macrobot.services.*
import me.nicholas.macrobot.util.bold

open class PermissionOverrideArg(override val name : String = "Macro Category/Command Name") : ArgumentType<OverrideName>() {
    companion object : PermissionOverrideArg()

    override fun generateExamples(event: CommandEvent<*>) = listOf("moderation", "utility", "addmacro")

    override fun convert(arg: String, args: List<String>, event: CommandEvent<*>): ArgumentResult<OverrideName> {
        val firstWord = arg.toLowerCase()

        val (databaseService, permissionService) = event.discord.getInjectionObjects(DatabaseService::class, PermissionService::class)

        event.container[firstWord]?.let {
            permissionService.getOrPutDefaultCommandPermission(firstWord)
        }

        databaseService.permissionOverrides[firstWord]
                ?: return ArgumentResult.Error("${bold(firstWord)} is not a valid command or a macro category")

        return ArgumentResult.Success(firstWord, 1)
    }
}