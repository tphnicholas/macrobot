package me.nicholas.macrobot.arguments

import me.jakejmattson.kutils.api.dsl.arguments.*
import me.jakejmattson.kutils.api.dsl.command.*

import me.nicholas.macrobot.services.DatabaseService
import me.nicholas.macrobot.util.bold

open class MacroArg(override val name : String = "Macro Name") : ArgumentType<String>() {
    companion object : MacroArg()

    override fun generateExamples(event: CommandEvent<*>) = listOf("ask", "sask", "wrapmini")

    override fun convert(arg: String, args: List<String>, event: CommandEvent<*>): ArgumentResult<String> {
        val firstWord = arg.toLowerCase()

        val databaseService = event.discord.getInjectionObjects(DatabaseService::class)

        if (!databaseService.hasMacroWithName(firstWord)) {
            return ArgumentResult.Error("${bold(firstWord)} is not a valid macro")
        }

        return ArgumentResult.Success(firstWord, 1)
    }
}