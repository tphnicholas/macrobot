package me.nicholas.macrobot.arguments

import me.jakejmattson.kutils.api.dsl.arguments.*
import me.jakejmattson.kutils.api.dsl.command.CommandEvent

import me.nicholas.macrobot.dataclasses.PermissionLevel
import me.nicholas.macrobot.extensions.map
import me.nicholas.macrobot.services.PermissionService
import me.nicholas.macrobot.util.bold

open class AccessiblePermissionLevelArg(override val name : String = "Accessible Permission Level") : PermissionLevelArg() {
    companion object : AccessiblePermissionLevelArg()

    override fun convert(arg: String, args: List<String>, event: CommandEvent<*>): ArgumentResult<PermissionLevel> {
        val member = event.message.member!!

        val permissionService = event.discord.getInjectionObjects(PermissionService::class)

        return super.convert(arg, args, event).map {
            val userPermissionLevel = permissionService.getMemberPermissionLevel(member)

            if (!permissionService.hasPermission(userPermissionLevel, it)) {
                return ArgumentResult.Error("${bold(it)} is higher than your permission level of ${bold(userPermissionLevel)}")
            }
        }
    }
}