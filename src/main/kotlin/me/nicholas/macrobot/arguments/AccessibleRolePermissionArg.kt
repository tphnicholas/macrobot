package me.nicholas.macrobot.arguments

import net.dv8tion.jda.api.entities.Role
import me.jakejmattson.kutils.api.dsl.arguments.*
import me.jakejmattson.kutils.api.dsl.command.*

import me.nicholas.macrobot.extensions.map
import me.nicholas.macrobot.services.*
import me.nicholas.macrobot.util.bold

open class AccessibleRolePermissionArg(override val name : String = "Accessible Role Permission") : RolePermissionArg() {
    companion object : AccessibleRolePermissionArg()

    override fun convert(arg: String, args: List<String>, event: CommandEvent<*>): ArgumentResult<Role> {
        val member = event.message.member!!

        val (databaseService, permissionService) = event.discord.getInjectionObjects(DatabaseService::class, PermissionService::class)

        return super.convert(arg, args, event).map {
            val rolePermissionLevel = databaseService.permissions[it.id]!!.level

            val userPermissionLevel = permissionService.getMemberPermissionLevel(member)

            if (!permissionService.hasPermission(userPermissionLevel, rolePermissionLevel)) {
                return ArgumentResult.Error("Role ${bold(it.name)} has a permission level of ${bold(rolePermissionLevel)} which is higher than your permission level of ${bold(userPermissionLevel)}")
            }
        }
    }
}