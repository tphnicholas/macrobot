package me.nicholas.macrobot.arguments

import me.jakejmattson.kutils.api.dsl.arguments.*
import me.jakejmattson.kutils.api.dsl.command.*
import me.nicholas.macrobot.dataclasses.CommandPermission
import me.nicholas.macrobot.dataclasses.MacroCategoryPermission

import me.nicholas.macrobot.dataclasses.OverrideName
import me.nicholas.macrobot.extensions.map
import me.nicholas.macrobot.services.*
import me.nicholas.macrobot.util.bold

open class AccessiblePermissionOverrideArg(override val name : String = "Accessible Command/Macro Category Name") : PermissionOverrideArg() {
    companion object : AccessiblePermissionOverrideArg()

    override fun convert(arg: String, args: List<String>, event: CommandEvent<*>): ArgumentResult<OverrideName> {
        val member = event.message.member!!

        val (databaseService, permissionService) = event.discord.getInjectionObjects(DatabaseService::class, PermissionService::class)

        return super.convert(arg, args, event).map {
            val permissionOverride = databaseService.permissionOverrides[it]!!

            val invocablePermissionLevel = permissionOverride.requiredPermissionLevel

            val userPermissionLevel = permissionService.getMemberPermissionLevel(member)

            if (!permissionService.hasPermission(userPermissionLevel, invocablePermissionLevel)) {
                when(permissionOverride) {
                    is CommandPermission -> return ArgumentResult.Error(
                            "Command ${bold(it)} has a permission level of ${bold(it)} which is higher than your permission level of ${bold(userPermissionLevel)}"
                    )
                    is MacroCategoryPermission -> return ArgumentResult.Error(
                            "Macro category ${bold(it)} has a permission level of ${bold(it)} which is higher than your permission level of ${bold(userPermissionLevel)}"
                    )
                }
            }
        }
    }
}