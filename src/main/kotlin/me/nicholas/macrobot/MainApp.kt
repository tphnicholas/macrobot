package me.nicholas.macrobot

import me.jakejmattson.kutils.api.dsl.configuration.startBot
import me.jakejmattson.kutils.api.services.ScriptEngineService

import me.nicholas.macrobot.data.Configuration

fun main(args: Array<String>) {
    val token = args.firstOrNull()
            ?: throw IllegalArgumentException("Expected bot token as a program argument")

    startBot(token) {
        val configuration = discord.getInjectionObjects(Configuration::class)
        val scriptService = ScriptEngineService(discord)

        registerInjectionObjects(scriptService)

        configure {
            prefix { configuration.prefix }
            requiresGuild = true
            commandReaction = null
        }
    }
}