package me.nicholas.macrobot.data

import me.jakejmattson.kutils.api.annotations.Data

import me.nicholas.macrobot.util.Constants

@Data(Constants.configFile, killIfGenerated = true)
data class Configuration(
        val prefix: String = "bot-prefix",
        val staffRoleId: String = "role-id"
)