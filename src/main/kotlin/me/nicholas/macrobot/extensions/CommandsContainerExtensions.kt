package me.nicholas.macrobot.extensions

import me.jakejmattson.kutils.api.dsl.command.CommandsContainer

fun CommandsContainer.hasCommandWithName(name: String) = (this[name] != null)