package me.nicholas.macrobot.extensions

import me.jakejmattson.kutils.api.dsl.command.CommandEvent
import me.jakejmattson.kutils.api.dsl.embed.EmbedDSLHandle

import me.nicholas.macrobot.services.DatabaseService
import me.nicholas.macrobot.dataclasses.*
import me.nicholas.macrobot.util.bold

fun EmbedDSLHandle.buildVisibleLimitsField(invocableName: String, event: CommandEvent<*>) {
    field {
        val databaseService = event.discord.getInjectionObjects(DatabaseService::class)

        val permissionOverride = databaseService.permissionOverrides[invocableName]!!

        val visibleLimits = event.getVisibleLimits(invocableName)

        when(permissionOverride) {
            is CommandPermission -> name = "Where can I invoke this command?"
            is MacroCategoryPermission -> name = "Where can I invoke this macro?"
        }

        if (visibleLimits == null) {
            value = "Nowhere."
        } else if (!permissionOverride.hasAnyLimitsSet()) {
            value = "Anywhere."
        } else {
            val (visibleCategories, visibleChannels) = visibleLimits

            if (visibleCategories.isNotEmpty()) {
                value = "In all channels under: ${visibleCategories.joinToString(", ") { bold(it.name) }}"
            }
            if (visibleChannels.isNotEmpty()) {
                value = (value + "\nIn channels: ${visibleChannels.joinToString(", ") { it.asMention }}")
            }
        }
    }
}