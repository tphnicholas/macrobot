package me.nicholas.macrobot.extensions

import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.*
import me.jakejmattson.kutils.api.dsl.command.*

import me.nicholas.macrobot.dataclasses.*
import me.nicholas.macrobot.services.*
import me.nicholas.macrobot.util.*

fun CommandEvent<*>.getVisibleCommands() : List<Command> {
    val member = message.member!!

    val permissionService = discord.getInjectionObjects(PermissionService::class)

    return container.commands
            .filter {
                permissionService.hasClearance(member,
                        permissionService.getOrPutDefaultCommandPermission(it))
            }
}

fun CommandEvent<*>.getVisibleMacros() : Map<MacroName, MacroData> {
    val member = message.member!!

    val (databaseService, permissionService) = discord.getInjectionObjects(DatabaseService::class, PermissionService::class)

    return databaseService.macros
            .filter {
                permissionService.hasClearance(member,
                        databaseService.permissionOverrides[it.value.category]!!)
            }
}

fun CommandEvent<*>.getVisibleCommandsFrom(channel: MessageChannel, category: Category?) : List<Command> {
    val permissionService = discord.getInjectionObjects(PermissionService::class)

    return getVisibleCommands()
            .filter {
                permissionService.checkLimitsFor(
                        channel, category,
                        permissionService.getOrPutDefaultCommandPermission(it)
                )
            }
}

fun CommandEvent<*>.getVisibleMacrosFrom(channel: MessageChannel, category: Category?) : Map<MacroName, MacroData> {
    val (databaseService, permissionService) = discord.getInjectionObjects(DatabaseService::class, PermissionService::class)

    return getVisibleMacros()
            .filterValues {
                permissionService.checkLimitsFor(
                        channel, category,
                        databaseService.permissionOverrides[it.category]!!
                )
            }
}

fun CommandEvent<*>.getVisibleLimits(name: String) : Pair<Set<Category>, Set<TextChannel>>? {
    val member = message.member!!
    val guild = guild!!

    val (databaseService, permissionService) = discord.getInjectionObjects(DatabaseService::class, PermissionService::class)

    val permissionOverride = databaseService.permissionOverrides[name]
            ?: return null

    if (!permissionService.hasClearance(member, permissionOverride)) {
        return null
    }

    val channelsWithinLimits = permissionOverride.channelLimits
    val categoriesWithinLimits = permissionOverride.categoryLimits

    val visibleCategories = mutableSetOf<Category>()
    val visibleChannels = guild.textChannels
            .filter { (it.id in channelsWithinLimits) || (it.parent?.id in categoriesWithinLimits) }
            .filter { member.hasPermission(it, Permission.MESSAGE_READ, Permission.MESSAGE_WRITE) }
            .toMutableSet()

    guild.categories
            .filter { it.id in categoriesWithinLimits }
            .map { it to it.textChannels }
            .forEach { (category, channels) ->
                if (channels.all { it in visibleChannels }) {
                    visibleChannels.removeAll(channels)
                    visibleCategories.add(category)
                }
            }

    return Pair(visibleCategories, visibleChannels)
}

fun CommandEvent<*>.parseForRequiredArguments(message: String) : Pair<List<SpecialArgument>, String>? {
    var messageDisplay = message

    val matches = specialArgumentSubscriptRegex
            .findAll(messageDisplay)
            .map {
                val (fullMatch, index, accessPath) = it.groupValues
                Triple(fullMatch, index.toIntOrNull(), accessPath)
            }

    val indexes = matches
            .mapNotNull { (_, index, _) -> index }
            .also {
                if (it.count() < matches.count()) {
                    return null.also { respond("At least one index is out of bounds") }
                }
            }
            .toSet()

    indexes.forEach {
        matches.filter { (_, index, _) -> index == it }
                .fold(0) { transformPos, (fullMatch, _, _) ->
                    messageDisplay = messageDisplay.replace(fullMatch, "{${it}.${transformPos}}")
                    transformPos + 1
        }
    }

    val highestIndex = indexes.max()
            ?: return Pair(emptyList(), message)

    val indexCount = indexes.size

    if (highestIndex >= indexCount) {
        return null.also { respond("At least one index is out of bounds") }
    }

    val messageParts = messageDisplay
            .split(' ', limit = indexCount + 1)

    val arguments = messageParts
            .take(indexCount)
            .map {
                val argumentParts = it.split('=', limit = 2)
                val typeName = argumentParts[0]
                val defaultValue = argumentParts
                        .getOrNull(1)
                        ?.let { if (it.isEmpty()) emptyList() else it.split(' ') }

                Pair(
                        getArgumentTypeName(typeName)?.first
                                ?: return null.also{ _ -> respond("${bold(it)} does not name an argument type") },
                        defaultValue
                )
            }

    (0..highestIndex)
            .find { it !in indexes }
            ?.let { return null.also { respond("At least one argument type provided is unused") } }

    return Pair(
            arguments.mapIndexed { pos, x ->
                val (qualifiedName, defaultValue) = x
                SpecialArgument(
                        qualifiedName,
                        matches.filter { (_, index, _) -> index == pos }
                                .map { (_, _, accessPath) -> accessPath }
                                .toMutableSet(),
                        defaultValue
                )
            },
            messageParts.last()
    )
}