package me.nicholas.macrobot.extensions

import me.jakejmattson.kutils.api.dsl.arguments.ArgumentResult

inline fun<T> ArgumentResult<T>.map(block: (T) -> Unit)
        = this.also { if (it is ArgumentResult.Success) { block(it.result) } }

fun<T> ArgumentResult<T>.maybeConverted() : T?
        = if (this is ArgumentResult.Success) result else null