package me.nicholas.macrobot.extensions

import me.jakejmattson.kutils.api.dsl.command.Command

fun Command.nameInLowercase() = names[0].toLowerCase()