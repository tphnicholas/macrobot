package me.nicholas.macrobot.listeners

import com.google.common.eventbus.Subscribe
import net.dv8tion.jda.api.events.role.RoleDeleteEvent

import me.nicholas.macrobot.services.*

class RoleDeletionListener(
        private val databaseService: DatabaseService,
        private val permissionService: PermissionService
) {
    @Subscribe
    fun onRoleDelete(event: RoleDeleteEvent) {
        val role = event.role

        databaseService.applyUpdate {
            permissionService.maybeRemovePermission(role.id)
        }
    }
}