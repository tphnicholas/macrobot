package me.nicholas.macrobot.listeners

import com.google.common.eventbus.Subscribe
import net.dv8tion.jda.api.events.channel.category.CategoryDeleteEvent

import me.nicholas.macrobot.services.DatabaseService

class CategoryDeletionListener(private val databaseService: DatabaseService) {
    @Subscribe
    fun onCategoryDelete(event: CategoryDeleteEvent) {
        val category = event.category

        databaseService.applyUpdate {
            permissionOverrides.values.forEach { it.categoryLimits.remove(category.id) }
        }
    }
}