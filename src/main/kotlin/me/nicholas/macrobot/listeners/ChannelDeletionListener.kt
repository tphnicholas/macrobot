package me.nicholas.macrobot.listeners

import com.google.common.eventbus.Subscribe
import net.dv8tion.jda.api.events.channel.text.TextChannelDeleteEvent

import me.nicholas.macrobot.services.DatabaseService

class ChannelDeletionListener(private val databaseService: DatabaseService) {
    @Subscribe
    fun onTextChannelDelete(event: TextChannelDeleteEvent) {
        val channel = event.channel

        databaseService.applyUpdate {
            permissionOverrides.values.forEach { it.channelLimits.remove(channel.id) }
        }
    }
}