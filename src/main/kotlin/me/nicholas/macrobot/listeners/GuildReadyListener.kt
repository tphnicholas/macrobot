package me.nicholas.macrobot.listeners

import kotlin.system.exitProcess
import com.google.common.eventbus.Subscribe
import net.dv8tion.jda.api.events.guild.GuildReadyEvent

import me.nicholas.macrobot.services.*
import me.nicholas.macrobot.util.bold

class GuildReadyListener(
        private val databaseService: DatabaseService,
        private val permissionService: PermissionService
) {
    @Subscribe
    fun onGuildReady(event: GuildReadyEvent) {
        val guild = event.guild

        guild.getRoleById(permissionService.defaultCommandPermission) ?: run {
            println("Role set as default command permission does not exist in guild ${bold(guild.name)}")
            exitProcess(-1)
        }

        databaseService.applyUpdate {
            permissions.keys.forEach { guild.getRoleById(it) ?: permissionService.removePermission(it) }

            permissionOverrides.values.forEach {
                it.categoryLimits.forEach { guild.getCategoryById(it) ?: permissionOverrides.remove(it) }

                it.channelLimits.forEach { guild.getGuildChannelById(it) ?: permissionOverrides.remove(it) }
            }
        }
    }
}