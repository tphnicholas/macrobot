package me.nicholas.macrobot.commands

import me.jakejmattson.kutils.api.annotations.CommandSet
import me.jakejmattson.kutils.api.arguments.*
import me.jakejmattson.kutils.api.dsl.command.*

import me.nicholas.macrobot.arguments.*
import me.nicholas.macrobot.dataclasses.*
import me.nicholas.macrobot.extensions.*
import me.nicholas.macrobot.util.*
import me.nicholas.macrobot.services.DatabaseService

@CommandSet("MacroCommands")
fun macroCommands(databaseService: DatabaseService) = commands {
    command("AddMacro") {
        description = "Adds new macro"
        execute(AnyArg("Macro Name"), AnyArg("Macro Category"), EveryArg("Macro Message")) {
            val name = it.args.component1().toLowerCase()
            val category = it.args.component2().toLowerCase()
            val messageRaw = it.args.component3()

            if (it.container.hasCommandWithName(name)) {
                return@execute it.respond("A command with the name ${bold(name)} already exists")
            }

            if (databaseService.hasMacroWithName(name)) {
                return@execute it.respond("A macro with the name ${bold(name)} already exists")
            }

            val (specialArguments, message) = it.parseForRequiredArguments(messageRaw)
                    ?: return@execute

            databaseService.applyUpdate {
                macros[name] = MacroData(category, message, specialArguments)
                permissionOverrides[category] = MacroCategoryPermission()
            }

            it.respond("Added macro ${bold(name)} in category ${bold(category)} responding with message ${bold(messageRaw.drop(specialArguments.size))}")
        }
    }

    command("RemoveMacro") {
        description = "Removes specified macro"
        execute(MacroArg) {
            val name = it.args.component1()

            val macro = databaseService.macros[name]!!

            databaseService.applyUpdate {
                macros.remove(name)
            }

            it.respond("Removed macro ${bold(name)} from category ${bold(macro.category)}")
        }
    }

    command("EditMessage") {
        description = "Edits message of specified macro"
        execute(MacroArg, EveryArg("New Message")) {
            val (name, messageRaw) = it.args

            val (specialArguments, message) = it.parseForRequiredArguments(messageRaw)
                    ?: return@execute

            databaseService.applyUpdate {
                macros[name]!!.specialArguments = specialArguments
                macros[name]!!.message = message
            }

            it.respond("Macro ${bold(name)} now responds with message ${bold(messageRaw.drop(specialArguments.size))}")
        }
    }

    command("EditCategory") {
        description = "Edits category of specified macro"
        execute(MacroArg, MacroCategoryArg) {
            val name = it.args.component1()
            val category = it.args.component2()

            databaseService.applyUpdate {
                macros[name]!!.category = category
            }

            it.respond("Macro ${bold(name)} now is under the category ${bold(category)}")
        }
    }
}