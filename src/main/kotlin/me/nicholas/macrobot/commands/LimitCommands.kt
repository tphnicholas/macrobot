package me.nicholas.macrobot.commands

import me.jakejmattson.kutils.api.annotations.CommandSet
import me.jakejmattson.kutils.api.arguments.*
import me.jakejmattson.kutils.api.dsl.command.*

import me.nicholas.macrobot.arguments.*
import me.nicholas.macrobot.services.*
import me.nicholas.macrobot.util.bold

@CommandSet("LimitCommands")
fun limitCommands(databaseService: DatabaseService) = commands {
    command("AddLimits") {
        description = "Add channel/category-specific limitation to specified command/macro category"
        execute(PermissionOverrideArg, MultipleArg(TextChannelArg or CategoryArg(allowsGlobal = true))) {
            val name = it.args.component1()
            val limits = it.args.component2()
            val permissionOverride = databaseService.permissionOverrides[name]!!

            databaseService.applyUpdate {
                limits.forEach { it.getData(
                        { permissionOverride.channelLimits.add(it.id) },
                        { permissionOverride.categoryLimits.add(it.id) }
                )}
            }

            it.respond("Added specified limits to ${bold(name)}")
        }
    }

    command("RemoveLimits") {
        description = "Removes given channel/category-specific limitations from specified command/macro category, if none are given all limitations are removed"
        execute(PermissionOverrideArg, MultipleArg(TextChannelArg or CategoryArg(allowsGlobal = true)).makeNullableOptional()) {
            val name = it.args.component1()
            val limits = it.args.component2()
            val permissionOverride = databaseService.permissionOverrides[name]!!

            databaseService.applyUpdate {
                limits ?: run {
                    it.respond("Removed all limits from ${bold(name)}")
                    return@applyUpdate permissionOverride.clearAllLimits()
                }

                limits.forEach { it.getData(
                        { permissionOverride.channelLimits.remove(it.id) },
                        { permissionOverride.categoryLimits.remove(it.id) }
                )}

                it.respond("Removed specified limits from ${bold(name)}")
            }
        }
    }
}