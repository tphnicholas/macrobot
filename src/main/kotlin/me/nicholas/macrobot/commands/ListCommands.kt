package me.nicholas.macrobot.commands

import me.jakejmattson.kutils.api.annotations.CommandSet
import me.jakejmattson.kutils.api.arguments.*
import me.jakejmattson.kutils.api.dsl.command.*

import me.nicholas.macrobot.arguments.MacroArg
import me.nicholas.macrobot.extensions.*
import me.nicholas.macrobot.services.*

@CommandSet("ListCommands")
fun listCommands(
        databaseService: DatabaseService,
        embedService: EmbedService
) = commands {
    command("Help") {
        description = "Displays help information"
        execute((CommandArg or MacroArg).makeNullableOptional()) {
            val invocable = it.args.component1()
                    ?: return@execute it.respond(
                            embedService.buildHelpEmbed(
                                    it.getVisibleCommands(),
                                    it.getVisibleMacros(),
                                    it
                            )
                    )

            it.respond(
                    invocable.getData(
                    { command -> embedService.buildCommandHelpEmbed(command, it) },
                    { macro -> embedService.buildMacroHelpEmbed(macro, it) }
            ))
        }
    }

    command("ListAvailable") {
        description = "Lists commands/macros available to the caller"
        execute(TextChannelArg.makeNullableOptional()) {
            val channel = it.args.component1()
                    ?: return@execute it.respond(
                            embedService.buildInvocableEmbed(
                                    it.getVisibleCommands(),
                                    it.getVisibleMacros()
                            )
                    )

            it.respond(
                embedService.buildInvocableEmbed(
                        it.getVisibleCommandsFrom(channel, channel.parent),
                        it.getVisibleMacrosFrom(channel, channel.parent)
                )
            )
        }
    }

    command("ListMacros") {
        description = "Lists macros available to the caller"
        execute(TextChannelArg.makeNullableOptional()) {
            val channel = it.args.component1()
                    ?: return@execute it.respond(
                            embedService.buildInvocableEmbed(
                                    emptyList(),
                                    it.getVisibleMacros()
                            )
                    )

            it.respond(
                    embedService.buildInvocableEmbed(
                            emptyList(),
                            it.getVisibleMacrosFrom(channel, channel.parent)
                    )
            )
        }
    }

    command("ListPermissionLevels") {
        description = "Lists permission levels"
        execute {
            val guild = it.guild!!

            it.respond(
                    embedService.buildPermissionLevelEmbed(
                            databaseService.permissions,
                            guild
                    )
            )
        }
    }
}