package me.nicholas.macrobot.commands

import me.jakejmattson.kutils.api.annotations.CommandSet
import me.jakejmattson.kutils.api.arguments.*
import me.jakejmattson.kutils.api.dsl.arguments.ArgumentResult
import me.jakejmattson.kutils.api.dsl.command.*

import me.nicholas.macrobot.arguments.*
import me.nicholas.macrobot.dataclasses.*
import me.nicholas.macrobot.services.*
import me.nicholas.macrobot.util.bold

@CommandSet("PermissionCommands")
fun permissionCommands(
        databaseService: DatabaseService,
        permissionService: PermissionService
) = commands {
    command("AddPermission") {
        description = "Adds new permission"
        execute(RoleArg, AccessiblePermissionLevelArg) {
            val (role, permissionLevel) = it.args

            databaseService.applyUpdate {

                if (permissions[role.id] != null) {
                    val result = AccessibleRolePermissionArg.convert(role.name, listOf(role.name), it)

                    when(result) {
                        is ArgumentResult.Error -> return@applyUpdate it.respond(result.error)
                        is ArgumentResult.Success -> {
                            permissionService.modifyPermissionLevel(role.id, permissionLevel)
                            return@applyUpdate it.respond("Moved existing permission for role ${bold(role.name)} at level ${bold(permissionLevel)}")
                        }
                    }
                }

                permissions[role.id] = PermissionData(permissionLevel)
                it.respond("Added permission for role ${bold(role.name)} at level ${bold(permissionLevel)}")
            }
        }
    }

    command("RemovePermission") {
        description = "Deletes permission"
        execute(AccessibleRolePermissionArg) {
            val role = it.args.component1()

            if (permissionService.isDefaultCommandPermission(role)) {
                return@execute it.respond("Cannot remove default command permission")
            }

            databaseService.applyUpdate {
                permissionService.removePermission(role.id)

                it.respond("Removed specified permission for role ${bold(role.name)}")
            }
        }
    }

    command("SetPermission") {
        description = "Sets permission level to specified command/macro category"
        execute(AccessiblePermissionOverrideArg, AccessibleRolePermissionArg) {
            val (name, role) = it.args

            val permission = databaseService.permissions[role.id]!!

            val permissionOverride = databaseService.permissionOverrides[name]!!

            databaseService.applyUpdate {
                permissionOverride.requiredPermissionLevel = permission.level
            }

            when(permissionOverride) {
                is CommandPermission -> it.respond("Command ${bold(name)} now has a permission level of ${bold(permission.level)}")
                is MacroCategoryPermission -> it.respond("All macros under category ${bold(name)} now have a permission level of ${bold(permission.level)}")
            }
        }
    }

    command("UnsetPermissions") {
        description = "Sets permission level to default for specified command/macro category"
        execute(AccessiblePermissionOverrideArg) {
            val name = it.args.component1()

            val permissionOverride = databaseService.permissionOverrides[name]!!

            databaseService.applyUpdate {
                permissionService.resetPermissionLevel(permissionOverride)
            }

            when(permissionOverride) {
                is CommandPermission -> it.respond("Command ${bold(name)} now has a permission level of ${bold(permissionOverride.requiredPermissionLevel ?: "None")}")
                is MacroCategoryPermission -> it.respond("All macros under category ${bold(name)} now have a permission level of ${bold(permissionOverride.requiredPermissionLevel ?: "None")}")
            }
        }
    }

    command("GetPermissionLevel") {
        description = "Displays permission level of specified role/command/macro category"
        execute(PermissionOverrideArg or RolePermissionArg) {
            val arg = it.args.component1()

            arg.getData(
                    { name ->
                        val permissionOverride = databaseService.permissionOverrides[name]!!

                        when(permissionOverride) {
                            is CommandPermission -> it.respond("Command ${bold(name)} has a permission level of ${bold(permissionOverride.requiredPermissionLevel ?: "None")}")
                            is MacroCategoryPermission -> it.respond("All macros under category ${bold(name)} have a permission level of ${bold(permissionOverride.requiredPermissionLevel ?: "None")}")
                        }
                    },
                    { role ->
                        val permission = databaseService.permissions[role.id]!!

                        it.respond("All users with the role ${bold(role.name)} have a permission level of ${bold(permission.level)}")
                    }
            )
        }
    }

    command("CompressLevels") {
        description = "Compresses permission levels to 0..N format while maintaining the relative hierarchy"
        execute {
            databaseService.applyUpdate {
                permissions.values
                        .groupByTo(sortedMapOf()) { it.level }
                        .values.forEachIndexed { newLevel, permissions ->
                            permissions.forEach { it.level = newLevel }
                        }

                permissionOverrides.values
                        .filter { it.requiredPermissionLevel != null }
                        .groupByTo(sortedMapOf()) { it.requiredPermissionLevel!! }
                        .values.forEachIndexed { newLevel, categories ->
                            categories.forEach { it.requiredPermissionLevel = newLevel }
                        }
            }

            it.respond("Compressed permission levels")
        }
    }
}