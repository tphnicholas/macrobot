# Commands

## Key
| Symbol     | Meaning                    |
| ---------- | -------------------------- |
| (Argument) | This argument is optional. |

## LimitCommands
| Commands     | Arguments                                                 | Description                                                                                                                              |
| ------------ | --------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------- |
| AddLimits    | Macro Category/Command Name, TextChannel \| Category...   | Add channel/category-specific limitation to specified command/macro category                                                             |
| RemoveLimits | Macro Category/Command Name, (TextChannel \| Category...) | Removes given channel/category-specific limitations from specified command/macro category, if none are given all limitations are removed |

## ListCommands
| Commands             | Arguments               | Description                                   |
| -------------------- | ----------------------- | --------------------------------------------- |
| Help                 | (Command \| Macro Name) | Displays help information                     |
| ListAvailable        | (TextChannel)           | Lists commands/macros available to the caller |
| ListMacros           | (TextChannel)           | Lists macros available to the caller          |
| ListPermissionLevels | <none>                  | Lists permission levels                       |

## MacroCommands
| Commands     | Arguments                                 | Description                       |
| ------------ | ----------------------------------------- | --------------------------------- |
| AddMacro     | Macro Name, Macro Category, Macro Message | Adds new macro                    |
| EditCategory | Macro Name, Macro Category                | Edits category of specified macro |
| EditMessage  | Macro Name, New Message                   | Edits message of specified macro  |
| RemoveMacro  | Macro Name                                | Removes specified macro           |

## PermissionCommands
| Commands           | Arguments                                                          | Description                                                                          |
| ------------------ | ------------------------------------------------------------------ | ------------------------------------------------------------------------------------ |
| AddPermission      | Role, Accessible Permission Level                                  | Adds new permission                                                                  |
| CompressLevels     | <none>                                                             | Compresses permission levels to 0..N format while maintaining the relative hierarchy |
| GetPermissionLevel | Macro Category/Command Name \| Role Permission                     | Displays permission level of specified role/command/macro category                   |
| RemovePermission   | Accessible Role Permission                                         | Deletes permission                                                                   |
| SetPermission      | Accessible Command/Macro Category Name, Accessible Role Permission | Sets permission level to specified command/macro category                            |
| UnsetPermissions   | Accessible Command/Macro Category Name                             | Sets permission level to default for specified command/macro category                |

